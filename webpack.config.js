var webpack = require ('webpack');
module.exports = {
	entry : './src/index.js',
	output: {
		filename : 'bundle.js',
		path: __dirname + '/dist'
	},
	devServer : {
		contentBase: __dirname + '/dist',
		compress: true,
		port: 9000
	},
	module: {
		rules: [
			{test: /\.js$/, exclude:/node_modules/, loader: 'babel-loader'},
			{test: /\.css$/, loader: 'style-loader!css-loader'},
			{test: /\.scss$/, exclude:/node_modules/, loader: 'style-loader!css-loader!sass-loader'},
			{test: /\.(eot|svg|ttf|woff|woff2)$/, loader: 'file-loader'}
		]
	},
	plugins: [
        new webpack.ProvidePlugin({
           $: "jquery",
           jQuery: "jquery"
       })
    ]
}